/*
Fichier décrivant les niveaux, les variables globales, ...
On a un tableau d'objet, chaque objet décrivant un nuveau
*/
var levels = [
	{
	  // 0 Avancer
	  // Canvas gauche
	  gauche: [
		  [0, 0, 0, 0, 0], [0, 0, 1, 0, 0], [0, 0, 1, 0, 0], [0, 0, 1, 0, 0],
		  [0, 0, 1, 0, 0], [0, 0, 1, 0, 0], [0, 0, 0, 0, 0]
	  ],
	  // Canvas droite
	  droite: [
		  [0, 0, 0, 0, 0], [0, 0, 1, 0, 0], [0, 0, 1, 0, 0], [0, 0, 1, 0, 0],
		  [0, 0, 1, 0, 0], [0, 0, 1, 0, 0], [0, 0, 0, 0, 0]
	  ],
	  // Position depart des bots
	  begin: [[5, 2], [5, 2]],
	  // Position de fin des bots
	  end: [[1, 2], [1, 2]],
	  // Angle de depart
	  angle: [0, 0],
	  // Liste des messages, positions, temps avant le prochain
	  messages: [
          {m: 'Niveau 1'},
		  {m: 'Amenez chaque robot à sa destination.', l: '60%', t: '60%', s: 500},
		  {
			m: 'Pour programmer votre robot, utilisez l\'icône <img src="./assets/img/gear_red.png" class="icone">',
			l: '20%',
			t: '40%',
			s: 1500
		  },
		  {
			m: 'Pour faire avancer votre robot, utilisez l\'instruction <img src="./assets/img/forward_red.png" class="icone">',
			l: '10%',
			t: 't'
		  },
		  {
			m: 'En cas d\'erreur, utilisez <img src="./assets/img/undo_red.png" class="icone"> pour effacer.',
			l: '10%',
			t: 't'
		  },
		  {
			m: 'Pour exécuter le programme, appuyez sur le bouton <img src="./assets/img/launch.png" class="icone">',
			l: '10%',
			t: 't'
		  },
		  {m: 'C\'est parti !', l: 'l', t: 't', s: 500}
	  ],
	  // Nombre d'actions pour reussir parfaitement
	  nbActionTotal: 8, 
      // instructions disponibles
      instructions: ['avancer'],
      groupe: 1
	},
	{
	  // 1 Avancer tourner
	  gauche: [
		  [0, 0, 0, 0, 0], [0, 3, 1, 1, 0], [0, 1, 0, 0, 0], [0, 1, 0, 0, 0],
		  [0, 3, 1, 2, 0], [0, 0, 0, 1, 0], [0, 0, 0, 1, 0]
	  ],
	  droite: [
		  [0, 0, 0, 0, 0], [0, 1, 1, 2, 0], [0, 0, 0, 1, 0], [0, 0, 0, 1, 0],
		  [0, 3, 1, 2, 0], [0, 1, 0, 0, 0], [0, 1, 0, 0, 0]
	  ],
	  begin: [[6, 3], [6, 1]],
	  end: [[1, 3], [1, 1]],
	  angle: [0, 0],
	  messages: [
		  {m: 'Niveau 2', l: '42%', t: '40%', s: 1000},
		  {m: 'Il va falloir tourner maintenant !', l: 'l', t: 't', s: 1500},
		  {m: '<img src="./assets/img/rotate_right_red.png" class="icone"> permet de tourner d\'un 1/4 de tour à droite', l: 'l', t: 't', s: 1500},
		  {m: '<img src="./assets/img/rotate_left_red.png" class="icone"> permet de tourner d\'un 1/4 de tour à gauche', l: 'l', t: 't', s: 1500},
		  {m: 'Attention : quand le robot tourne, il reste sur place.', l: 'l', t: 't', s: 1500},
		  {m: 'Les motifs au sol sont là pour vous aider.', l: 'l', t: 't'},
		  {m: 'A vous de jouer !', l: 'l', t: 't'}
	  ],
	  nbActionTotal: 24, 
      // instructions disponibles
      instructions: ['avancer','rotateLeft','rotateRight'],
      groupe: 1
	},
	{
	  // 2 Avancer tourner changement de vue
	  gauche: [
		  [0, 0, 0, 0, 0], [0, 1, 0, 1, 0], [0, 1, 0, 1, 0], [0, 1, 0, 1, 0],
		  [0, 3, 1, 3, 0], [0, 0, 0, 0, 0], [0, 0, 0, 0, 0]
	  ],
	  droite: [
		  [0, 0, 0, 0, 0], [0, 1, 0, 1, 0], [0, 1, 0, 1, 0], [0, 1, 0, 1, 0],
		  [0, 2, 1, 2, 0], [0, 0, 0, 0, 0], [0, 0, 0, 0, 0]
	  ],
	  begin: [[1, 3], [1, 1]],
	  end: [[1, 1], [1, 3]],
	  angle: [180, 180],
	  messages: [{m: 'Niveau 3', l: '40%', t: '40%'}],
	  nbActionTotal: 20, 
      instructions: ['avancer','rotateLeft','rotateRight'],
      groupe: 1
	},
	{
	  // 3 Avancer tourner compter
	  gauche: [
		  [3, 1, 1, 1, 0], [1, 0, 0, 0, 0], [1, 0, 0, 0, 0], [3, 1, 1, 2, 0],
		  [0, 0, 0, 1, 0], [0, 3, 1, 2, 0], [0, 1, 0, 0, 0]
	  ],
	  droite: [
		  [0, 1, 1, 1, 2], [0, 0, 0, 0, 1], [0, 0, 0, 0, 1], [0, 3, 1, 1, 2],
		  [0, 1, 0, 0, 0], [0, 3, 1, 2, 0], [0, 0, 0, 1, 0]
	  ],
	  begin: [[6, 1], [6, 3]],
	  end: [[0, 3], [0, 1]],
	  angle: [0, 0],
	  messages: [{m: 'Niveau 4', l: '40%', t: '40%'}],
      instructions: ['avancer','rotateLeft','rotateRight'],
	  nbActionTotal: 38, 
      groupe: 1
	},
	{
	  // 4 Avancer tourner compter changement de vue
	  gauche: [
		  [0, 0, 0, 0, 0], [1, 0, 1, 0, 0], [1, 0, 1, 0, 0], [1, 0, 3, 1, 2],
		  [1, 0, 0, 0, 1], [2, 1, 1, 1, 2], [0, 0, 0, 0, 0]
	  ],
	  droite: [
		  [0, 0, 0, 0, 0], [0, 0, 1, 0, 1], [0, 0, 1, 0, 1], [3, 1, 2, 0, 1],
		  [1, 0, 0, 0, 1], [3, 1, 1, 1, 3], [0, 0, 0, 0, 0]
	  ],
	  begin: [[1, 0], [1, 4]],
	  end: [[1, 2], [1, 2]],
	  angle: [180, 180],
	  messages: [{m: 'Niveau 5', l: '40%', t: '40%'}],
	  instructions: ['avancer','rotateLeft','rotateRight'],
	  nbActionTotal: 36, 
      groupe: 1
	},
	{
	  // 5 plaque porte
	  // Canvas gauche
	  gauche: [
		  [0, 0, 0, 0, 0], [0, 0, 1, 0, 0], [0, 0, 1, 0, 0], [0, 0, 1, 0, 0],
		  [0, 0, 1, 0, 0], [0, 0, 1, 0, 0], [0, 0, 0, 0, 0]
	  ],
	  // Canvas droite
	  droite: [
		  [0, 0, 0, 0, 0], [0, 0, 1, 0, 0], [0, 0, 1, 0, 0], [0, 0, 1, 0, 0],
		  [0, 0, 1, 0, 0], [0, 0, 1, 0, 0], [0, 0, 0, 0, 0]
	  ],
	  // Position depart des bots
	  begin: [[5, 2], [5, 2]],
	  // Position de fin des bots
	  end: [[1, 2], [1, 2]],
	  // Angle de depart
	  angle: [0, 0],
	  // Liste des messages, positions, temps avant le prochain
	  messages: [ 
          {m: 'Niveau 6', l: '40%', t: '40%'},
          {m: 'Les barrières électriques se désactivent en plaçant un robot sur l\'interrupteur de la même couleur.', l: '40%', t: '40%'},
          {m: 'L\'instruction <img src="./assets/img/rest_red.png" class="icone"> permet de faire attendre le robot.', l: '40%', t: '40%'}
      
      ],
	  duoPreDoor:
		  [{pcvs: 2, pi: 2, pj: 4, pOn: 0, dcvs: 1, di: 2, dj: 3, dAngle: 0}],
	  instructions: ['avancer','rotateLeft','rotateRight','rest'],
	  nbActionTotal: 11, 
      groupe: 2
	},
	{
	  // 6 2 plaques portes fluide
	  // Canvas gauche
	  gauche: [
		  [0, 0, 0, 0, 0], [0, 1, 1, 2, 0], [0, 0, 0, 1, 0], [0, 0, 0, 1, 0],
		  [1, 1, 1, 2, 0], [0, 0, 0, 0, 0], [0, 0, 0, 0, 0]
	  ],
	  // Canvas droite
	  droite: [
		  [0, 0, 0, 0, 0], [0, 3, 1, 1, 0], [0, 1, 0, 0, 0], [0, 1, 0, 0, 0],
		  [0, 3, 1, 2, 0], [0, 0, 0, 1, 0], [0, 0, 0, 0, 0]
	  ],
	  // Position depart des bots
	  begin: [[4, 0], [5, 3]],
	  // Position de fin des bots
	  end: [[1, 1], [1, 3]],
	  // Angle de depart
	  angle: [90, 0],
	  // Liste des messages, positions, temps avant le prochain
	  messages: [{m: 'Niveau 7', l: '35%', t: '40%'}],
	  duoPreDoor: [
		  {pcvs: 1, pi: 3, pj: 4, pOn: 0, dcvs: 2, di: 2, dj: 4, dAngle: 90},
		  {pcvs: 2, pi: 1, pj: 1, pOn: 0, dcvs: 1, di: 2, dj: 1, dAngle: 90}
	  ],
	  nbActionTotal: 21, 
      instructions: ['avancer','rotateLeft','rotateRight','rest'],
	  groupe: 2
	},
	{
	  // 7 2 plaques portes
	  // Canvas gauche
	  gauche: [
		  [0, 0, 0, 0, 0], [0, 1, 2, 0, 0], [0, 0, 1, 0, 0], [0, 0, 1, 0, 0],
		  [0, 0, 1, 0, 0], [0, 0, 1, 0, 0], [0, 0, 0, 0, 0]
	  ],
	  // Canvas droite
	  droite: [
		  [0, 0, 0, 0, 0], [0, 3, 1, 1, 0], [0, 1, 0, 0, 0], [0, 1, 0, 0, 0],
		  [0, 3, 1, 2, 0], [0, 0, 0, 1, 0], [0, 0, 0, 0, 0]
	  ],
	  // Position depart des bots
	  begin: [[5, 2], [5, 3]],
	  // Position de fin des bots
	  end: [[1, 1], [1, 3]],
	  // Angle de depart
	  angle: [00, 0],
	  // Liste des messages, positions, temps avant le prochain
	  messages: [{m: 'Niveau 8', l: '40%', t: '40%'}],
	  duoPreDoor: [
		  {pcvs: 1, pi: 2, pj: 4, pOn: 0, dcvs: 2, di: 3, dj: 4, dAngle: 135},
		  {pcvs: 2, pi: 1, pj: 3, pOn: 0, dcvs: 1, di: 2, dj: 1, dAngle: 135}
	  ],
	  nbActionTotal: 22, 
      instructions: ['avancer','rotateLeft','rotateRight','rest'],
	  groupe: 2
	},
	{
	  // 8 Dance Revolution
	  // Canvas gauche
	  gauche: [
		  [0, 0, 0, 0, 0], [0, 0, 1, 0, 0], [0, 1, 1, 1, 0], [0, 0, 1, 0, 0],
		  [0, 0, 0, 0, 0], [0, 0, 0, 0, 0], [0, 0, 0, 0, 0]
	  ],
	  // Canvas droite
	  droite: [
		  [0, 0, 0, 0, 0], [0, 1, 1, 1, 0], [0, 1, 0, 0, 0], [0, 1, 1, 1, 0],
		  [0, 0, 0, 1, 0], [0, 1, 1, 1, 0], [0, 0, 0, 0, 0]
	  ],
	  // Position depart des bots
	  begin: [[2, 2], [1, 3]],
	  // Position de fin des bots
	  end: [[1, 2], [5, 1]],
	  // Angle de depart
	  angle: [180, 270],
	  // Liste des messages, positions, temps avant le prochain
	  messages: [
		  {m: 'Niveau 9', l: '40%', t: '40%', s: 1000},
		  {m: 'Dance Revolution !!', l: 'l', t: 't'}
	  ],
	  duoPreDoor: [
		  {pcvs: 1, pi: 1, pj: 2, pOn: 0, dcvs: 2, di: 1, dj: 1, dAngle: 225},
		  {pcvs: 1, pi: 3, pj: 2, pOn: 0, dcvs: 2, di: 2, dj: 3, dAngle: 90},
		  {pcvs: 1, pi: 2, pj: 3, pOn: 0, dcvs: 2, di: 3, dj: 5, dAngle: 135}
	  ],
	  nbActionTotal: 38, 
      instructions: ['avancer','rotateLeft','rotateRight','rest'],
	  groupe: 2
	},
	{
	  // 9 3 portes + choix
	  // Canvas gauche
	  gauche: [
		  [0, 0, 1, 0, 0], [0, 0, 1, 0, 0], [0, 0, 1, 0, 0], [0, 1, 1, 1, 0],
		  [0, 1, 0, 1, 0], [0, 1, 1, 1, 0], [0, 0, 0, 0, 0]
	  ],
	  // Canvas droite
	  droite: [
		  [0, 0, 0, 0, 0], [0, 1, 1, 1, 0], [0, 1, 0, 1, 0], [0, 1, 1, 1, 0],
		  [0, 0, 1, 0, 0], [0, 0, 1, 0, 0], [0, 0, 1, 0, 0]
	  ],
	  // Position depart des bots
	  begin: [[0, 2], [6, 2]],
	  // Position de fin des bots
	  end: [[5, 2], [1, 2]],
	  // Angle de depart
	  angle: [180, 0],
	  // Liste des messages, positions, temps avant le prochain
	  messages: [{m: 'Niveau 10', l: '40%', t: '40%'}],
	  duoPreDoor: [
		  {pcvs: 2, pi: 2, pj: 4, pOn: 0, dcvs: 1, di: 2, dj: 3, dAngle: 0},
		  {pcvs: 1, pi: 1, pj: 5, pOn: 0, dcvs: 2, di: 3, dj: 2, dAngle: 0},
		  {pcvs: 1, pi: 3, pj: 5, pOn: 0, dcvs: 2, di: 1, dj: 2, dAngle: 0}
	  ],
	  instructions: ['avancer','rotateLeft','rotateRight','rest'],
	  nbActionTotal: 33, 
      groupe: 2
	},
	{
	  // 10 boit + plaque
	  // Canvas gauche
	  gauche: [
		  [0, 0, 1, 0, 0], [0, 0, 1, 0, 0], [0, 0, 1, 0, 0], [0, 0, 1, 0, 0],
		  [0, 0, 1, 0, 0], [0, 0, 1, 0, 0], [0, 0, 1, 0, 0]
	  ],
	  // Canvas droite
	  droite: [
		  [0, 0, 1, 0, 0], [0, 0, 1, 0, 0], [0, 0, 1, 0, 0], [0, 0, 1, 0, 0],
		  [0, 0, 1, 0, 0], [0, 0, 1, 0, 0], [0, 0, 1, 0, 0]
	  ],
	  // Position depart des bots
	  begin: [[0, 2], [6, 2]],
	  // Position de fin des bots
	  end: [[6, 2], [0, 2]],
	  // Angle de depart
	  angle: [180, 0],
	  // Liste des messages, positions, temps avant le prochain
	  messages: [
		  {m: 'Niveau 11', l: '40%', t: '40%', s: 1000},
		  {m: 'Tu peux ramasser des boîtes...', l: 'l', t: 't', s: 1000},
		  {m: 'et les déposer sur des plaques pour débloquer les barrières.', l: 'l', t: 't', s: 1000},
		  {m: 'L\'instruction <img src="./assets/img/pickup_red.png" class="icone"> permet de prendre/déposer la boîte sur la case où se trouve le robot', l: 'l', t: 't', s: 1000}
	  ],
	  duoPreDoor: [
		  {pcvs: 1, pi: 2, pj: 2, pOn: 0, dcvs: 1, di: 2, dj: 4, dAngle: 0},
		  {pcvs: 2, pi: 2, pj: 4, pOn: 0, dcvs: 2, di: 2, dj: 2, dAngle: 0},
	  ],
	  boxes: [
		  {cvs: 1, i: 2, j: 1, droped: true},
		  {cvs: 2, i: 2, j: 5, droped: true}
	  ],
	  nbActionTotal: 16, 
      groupe: 3
	},
	{
	  // 11 boit + plaque
	  // Canvas gauche
	  gauche: [
		  [0, 0, 0, 0, 0], [0, 0, 1, 0, 0], [0, 0, 1, 0, 0], [0, 1, 1, 1, 0],
		  [0, 0, 1, 0, 0], [0, 0, 1, 0, 0], [0, 0, 0, 0, 0]
	  ],
	  // Canvas droite
	  droite: [
		  [0, 0, 0, 0, 0], [0, 0, 1, 0, 0], [0, 0, 1, 0, 0], [0, 1, 1, 1, 0],
		  [0, 0, 1, 0, 0], [0, 0, 1, 0, 0], [0, 0, 0, 0, 0]
	  ],
	  // Position depart des bots
	  begin: [[5, 2], [5, 2]],
	  // Position de fin des bots
	  end: [[1, 2], [1, 2]],
	  // Angle de depart
	  angle: [0, 0],
	  // Liste des messages, positions, temps avant le prochain
	  messages: [{m: 'Niveau 12', l: '40%', t: '40%'}],
	  duoPreDoor: [
		  {pcvs: 1, pi: 1, pj: 3, pOn: 0, dcvs: 2, di: 2, dj: 2, dAngle: 0},
		  {pcvs: 2, pi: 3, pj: 3, pOn: 0, dcvs: 1, di: 2, dj: 2, dAngle: 0}
	  ],
	  boxes: [
		  {cvs: 1, i: 3, j: 3, droped: true},
		  {cvs: 2, i: 1, j: 3, droped: true}
	  ],
	  nbActionTotal: 32, 
      groupe: 3
	},
	{
	  // 12 4 boites + 4 plaques
	  // Canvas gauche
	  gauche: [
		  [0, 0, 1, 0, 0], [0, 0, 1, 0, 0], [0, 0, 1, 0, 0], [0, 1, 1, 1, 0],
		  [0, 1, 1, 1, 0], [0, 1, 1, 1, 0], [0, 0, 0, 0, 0]
	  ],
	  // Canvas droite
	  droite: [
		  [0, 0, 1, 0, 0], [0, 0, 1, 0, 0], [0, 0, 1, 0, 0], [0, 1, 1, 1, 0],
		  [0, 1, 1, 1, 0], [0, 1, 1, 1, 0], [0, 0, 0, 0, 0]
	  ],
	  // Position depart des bots
	  begin: [[5, 3], [5, 1]],
	  // Position de fin des bots
	  end: [[0, 2], [0, 2]],
	  // Angle de depart
	  angle: [0, 0],
	  // Liste des messages, positions, temps avant le prochain
	  messages: [{m: 'Niveau 13', l: '40%', t: '40%'}],
	  duoPreDoor: [
		  {pcvs: 1, pi: 3, pj: 3, pOn: 0, dcvs: 2, di: 2, dj: 2, dAngle: 0},
		  {pcvs: 1, pi: 1, pj: 4, pOn: 0, dcvs: 1, di: 2, dj: 2, dAngle: 0},
		  {pcvs: 2, pi: 3, pj: 4, pOn: 0, dcvs: 1, di: 2, dj: 1, dAngle: 0},
		  {pcvs: 2, pi: 1, pj: 3, pOn: 0, dcvs: 2, di: 2, dj: 1, dAngle: 0}
	  ],
	  boxes: [
		  {cvs: 1, i: 1, j: 3, droped: true},
		  {cvs: 1, i: 3, j: 4, droped: true},
		  {cvs: 2, i: 3, j: 3, droped: true},
		  {cvs: 2, i: 1, j: 4, droped: true}
	  ],
	  nbActionTotal: 36, 
      groupe: 3
	},
	{
	  // 13 Peut pas prendre 2 boites
	  // Canvas gauche
	  gauche: [
		  [0, 0, 0, 0, 0], [0, 0, 1, 0, 0], [1, 1, 1, 1, 1], [0, 0, 1, 0, 0],
		  [0, 1, 1, 1, 0], [1, 1, 1, 1, 1], [1, 1, 1, 1, 1]
	  ],
	  // Canvas droite
	  droite: [
		  [0, 0, 0, 0, 0], [0, 0, 1, 0, 0], [1, 1, 1, 1, 1], [0, 0, 1, 0, 0],
		  [0, 1, 1, 1, 0], [1, 1, 1, 1, 1], [1, 1, 1, 1, 1]
	  ],
	  // Position depart des bots
	  begin: [[6, 2], [6, 2]],
	  // Position de fin des bots
	  end: [[2, 0], [2, 4]],
	  // Angle de depart
	  angle: [0, 0],
	  // Liste des messages, positions, temps avant le prochain
	  messages: [{m: 'Niveau 14', l: '40%', t: '40%'}],
	  duoPreDoor: [
		  {pcvs: 1, pi: 2, pj: 3, pOn: 0, dcvs: 1, di: 0, dj: 2, dAngle: 90},
		  {pcvs: 1, pi: 2, pj: 2, pOn: 0, dcvs: 2, di: 4, dj: 2, dAngle: 90},
		  {pcvs: 2, pi: 2, pj: 2, pOn: 0, dcvs: 1, di: 1, dj: 2, dAngle: 90},
		  {pcvs: 2, pi: 2, pj: 3, pOn: 0, dcvs: 2, di: 3, dj: 2, dAngle: 90}
	  ],
	  boxes: [
		  {cvs: 1, i: 2, j: 4, droped: true},
		  {cvs: 1, i: 2, j: 5, droped: true},
		  {cvs: 2, i: 2, j: 4, droped: true},
		  {cvs: 2, i: 2, j: 5, droped: true}
	  ],
	  nbActionTotal: 34, 
      groupe: 3
	},
	{
	  // 14
	  // Canvas gauche
	  gauche: [
		  [0, 0, 0, 0, 0], [0, 1, 1, 1, 0], [0, 1, 0, 0, 0], [0, 1, 0, 0, 0], [0, 1, 1, 1, 0],
		  [0, 1, 1, 1, 0], [0, 1, 1, 1, 0]
	  ],
	  // Canvas droite
	  droite: [
		  [0, 0, 0, 0, 0], [0, 1, 1, 1, 0], [0, 0, 0, 1, 0], [0, 0, 0, 1, 0], [0, 1, 1, 1, 0],
		  [0, 1, 1, 1, 0], [0, 1, 1, 1, 0]
	  ],
	  // Position depart des bots
	  begin: [[6, 2], [6, 2]],
	  // Position de fin des bots
	  end: [[1, 3], [1, 1]],
	  // Angle de depart
	  angle: [0, 0],
	  // Liste des messages, positions, temps avant le prochain
	  messages: [{m: 'Niveau 15', l: '40%', t: '40%'}],
	  duoPreDoor: [
		  {pcvs: 1, pi: 1, pj: 4, pOn: 0, dcvs: 2, di: 3, dj: 3, dAngle: 0},
		  {pcvs: 2, pi: 3, pj: 4, pOn: 0, dcvs: 1, di: 1, dj: 3, dAngle: 0},
		  {pcvs: 1, pi: 1, pj: 1, pOn: 0, dcvs: 2, di: 2, dj: 1, dAngle: 90},
		  {pcvs: 2, pi: 3, pj: 1, pOn: 0, dcvs: 1, di: 2, dj: 1, dAngle: 90}
	  ],
	  boxes: [
		  {cvs: 1, i: 2, j: 5, droped: true},
		  {cvs: 2, i: 2, j: 5, droped: true},
		  {cvs: 1, i: 1, j: 2, droped: true},
		  {cvs: 2, i: 3, j: 2, droped: true}
	  ],
	  nbActionTotal: 30, 
      groupe: 3
	},
	{
	  // 15 teleporteur + boite
	  // Canvas gauche
	  gauche: [
		  [0, 0, 1, 0, 0], [0, 0, 1, 0, 0], [0, 0, 1, 0, 0], [0, 0, 1, 0, 0],
		  [0, 0, 1, 0, 0], [0, 0, 1, 0, 0], [0, 0, 0, 0, 0]
	  ],
	  // Canvas droite
	  droite: [
		  [0, 0, 1, 0, 0], [0, 0, 1, 0, 0], [0, 0, 1, 0, 0], [0, 0, 1, 0, 0],
		  [0, 0, 1, 0, 0], [0, 0, 1, 0, 0], [0, 0, 1, 0, 0]
	  ],
	  // Position depart des bots
	  begin: [[5, 2], [6, 2]],
	  // Position de fin des bots
	  end: [[0, 2], [0, 2]],
	  // Angle de depart
	  angle: [0, 0],
	  // Liste des messages, positions, temps avant le prochain
	  messages: [
          {m: 'Niveau 16', l: '40%', t: '40%'},
          {m: 'Tu peux passer des boîtes à l\'autre robot avec les téléporteurs.', l: 'l', t: 't', s: 1000},
          {m: 'Dépose la boîte sur l\'entrée du téléporteur pour la transférer de l\'autre côté.', l: 'l', t: 't', s: 1000}
      ],
	  // Boites
	  boxes: [{cvs: 1, i: 2, j: 4, droped: true}],
	  // Teleporteurs
	  teleporters: [{
		  inCvs: 1,
		  inI: 2,
		  inJ: 3,
		  angleIn: 0,
		  outCvs: 2,
		  outI: 2,
		  outJ: 3,
		  angleOut: 0
	  }],
	  // Porte
	  duoPreDoor:
		  [{pcvs: 2, pi: 2, pj: 2, pOn: 0, dcvs: 2, di: 2, dj: 1, dAngle: 0}],
	  // Nombre d'actions pour reussir parfaitement
	  nbActionTotal: 15, 
      groupe: 4
	},
	{
	  // 16 1 boxs 1 teleporteurs <3
	  // Canvas gauche
	  gauche: [
		  [0, 0, 0, 0, 0], [0, 0, 0, 1, 0], [0, 0, 1, 1, 1], [0, 1, 1, 1, 1],
		  [0, 0, 1, 1, 1], [0, 0, 0, 1, 1], [0, 0, 0, 0, 1]
	  ],
	  // Canvas droite
	  droite: [
		  [0, 0, 0, 0, 0], [0, 1, 0, 0, 0], [1, 1, 1, 0, 0], [1, 1, 1, 1, 0],
		  [1, 1, 1, 0, 0], [1, 1, 0, 0, 0], [1, 0, 0, 0, 0]
	  ],
	  // Position depart des bots
	  begin: [[6, 4], [6, 0]],
	  // Position de fin des bots
	  end: [[1, 3], [1, 1]],
	  // Angle de depart
	  angle: [0, 0],
	  // Liste des messages, positions, temps avant le prochain
	  messages: [
		  {m: 'Niveau 17', l: '40%', t: '40%'},
	  ],
	  duoPreDoor:
		  [{pcvs: 2, pi: 1, pj: 3, pOn: 0, dcvs: 2, di: 1, dj: 2, dAngle: 0}],
	  boxes: [{cvs: 1, i: 4, j: 5, droped: true}],
	  teleporters: [{
		  inCvs: 1,
		  inI: 3,
		  inJ: 4,
		  angleIn: 0,
		  outCvs: 2,
		  outI: 1,
		  outJ: 4,
		  angleOut: 0
	  }],
	  nbActionTotal: 20, 
      groupe: 4
	},
	{
	  // 16 2 boxs 2 teleporteurs
	  // Canvas gauche
	  gauche: [
		  [0, 0, 0, 0, 1], [0, 0, 0, 0, 1], [0, 0, 0, 0, 1], [1, 1, 1, 1, 1],
		  [1, 0, 0, 0, 0], [1, 0, 0, 0, 0], [0, 0, 0, 0, 0]
	  ],
	  // Canvas droite
	  droite: [
		  [1, 0, 0, 0, 0], [1, 0, 0, 0, 0], [1, 0, 0, 0, 0], [1, 1, 1, 1, 1],
		  [0, 0, 0, 0, 1], [0, 0, 0, 0, 1], [0, 0, 0, 0, 1]
	  ],
	  // Position depart des bots
	  begin: [[5, 0], [6, 4]],
	  // Position de fin des bots
	  end: [[0, 4], [0, 0]],
	  // Angle de depart
	  angle: [0, 0],
	  // Liste des messages, positions, temps avant le prochain
	  messages: [
		  {m: 'Niveau 18', l: '40%', t: '40%'},
	  ],
	  duoPreDoor: [
		  {pcvs: 2, pi: 3, pj: 3, pOn: 0, dcvs: 2, di: 2, dj: 3, dAngle: 90},
		  {pcvs: 1, pi: 4, pj: 1, pOn: 0, dcvs: 1, di: 4, dj: 0, dAngle: 0}
	  ],
	  boxes: [
		  {cvs: 1, i: 0, j: 5, droped: true},
		  {cvs: 2, i: 2, j: 3, droped: true}
	  ],
	  teleporters: [
		  {
			inCvs: 1,
			inI: 0,
			inJ: 4,
			angleIn: 0,
			outCvs: 2,
			outI: 4,
			outJ: 3,
			angleOut: 0
		  },
		  {
			inCvs: 2,
			inI: 1,
			inJ: 3,
			angleIn: 0,
			outCvs: 1,
			outI: 4,
			outJ: 2,
			angleOut: 0
		  }
	  ],
	  nbActionTotal: 31, 
      groupe: 4
	},
	{
	  // 18 2 boxs 2 teleporteurs
	  // Canvas gauche
	  gauche: [
		  [0, 0, 0, 0, 0], [0, 0, 0, 0, 0], [0, 1, 1, 1, 0], [1, 1, 1, 1, 1],
		  [0, 1, 1, 1, 0], [0, 0, 0, 0, 0], [0, 0, 0, 0, 0]
	  ],
	  // Canvas droite
	  droite: [
		  [0, 0, 0, 0, 0], [0, 0, 0, 0, 0], [0, 1, 0, 1, 0], [1, 1, 1, 1, 1],
		  [0, 1, 0, 1, 0], [0, 0, 0, 0, 0], [0, 0, 0, 0, 0]
	  ],
	  // Position depart des bots
	  begin: [[3, 4], [3, 0]],
	  // Position de fin des bots
	  end: [[3, 0], [3, 4]],
	  // Angle de depart
	  angle: [270, 90],
	  // Liste des messages, positions, temps avant le prochain
	  messages: [
		  {m: 'Niveau 19', l: '40%', t: '40%'},
	  ],
	  duoPreDoor: [
		  {pcvs: 1, pi: 3, pj: 3, pOn: 0, dcvs: 2, di: 2, dj: 3, dAngle: 90},
		  {pcvs: 1, pi: 1, pj: 2, pOn: 0, dcvs: 1, di: 1, dj: 3, dAngle: 90}
	  ],
	  boxes: [{cvs: 2, i: 1, j: 3, droped: true}],
	  teleporters: [{
		  inCvs: 2,
		  inI: 3,
		  inJ: 3,
		  angleIn: 0,
		  outCvs: 1,
		  outI: 2,
		  outJ: 2,
		  angleOut: 0
	  }],
	  nbActionTotal: 20, 
      groupe: 4
	},
	{
	  // 19 2 boxs 2 teleporteurs
	  // Canvas gauche
	  gauche: [
		  [0, 1, 1, 1, 0], [0, 1, 1, 1, 0], [0, 1, 1, 1, 0], [1, 1, 1, 1, 1],
		  [1, 0, 1, 0, 0], [0, 0, 1, 0, 0], [0, 0, 1, 0, 0]
	  ],
	  // Canvas droite
	  droite: [
		  [0, 0, 1, 0, 0], [0, 0, 1, 0, 1], [0, 0, 1, 0, 1], [0, 1, 1, 1, 1],
		  [0, 1, 1, 1, 0], [0, 1, 1, 1, 0], [0, 0, 0, 0, 0]
	  ],
	  // Position depart des bots
	  begin: [[6, 2], [5, 2]],
	  // Position de fin des bots
	  end: [[4, 0], [1, 4]],
	  // Angle de depart
	  angle: [0, 0],
	  // Liste des messages, positions, temps avant le prochain
	  messages: [
		  {m: 'Niveau 20', l: '40%', t: '40%'},
	  ],
	  duoPreDoor: [
		  {pcvs: 2, pi: 2, pj: 3, pOn: 0, dcvs: 2, di: 2, dj: 2, dAngle: 0},
		  {pcvs: 1, pi: 2, pj: 2, pOn: 0, dcvs: 2, di: 4, dj: 2, dAngle: 0},
		  {pcvs: 1, pi: 3, pj: 0, pOn: 0, dcvs: 2, di: 4, dj: 3, dAngle: 45},
		  {pcvs: 2, pi: 4, pj: 2, pOn: 0, dcvs: 1, di: 0, dj: 3, dAngle: 45}
	  ],
	  boxes: [
		  {cvs: 2, i: 2, j: 4, droped: true},
		  {cvs: 2, i: 2, j: 1, droped: true},
		  {cvs: 2, i: 4, j: 3, droped: true}
	  ],
	  teleporters: [
		  {
			inCvs: 2,
			inI: 2,
			inJ: 0,
			angleIn: 0,
			outCvs: 1,
			outI: 2,
			outJ: 0,
			angleOut: 0
		  },
		  {
			inCvs: 2,
			inI: 3,
			inJ: 3,
			angleIn: 0,
			outCvs: 1,
			outI: 2,
			outJ: 1,
			angleOut: 0
		  }
	  ],
	  nbActionTotal: 51, 
      groupe: 4
	}
];

/* --- Valeur ---
- 0 : vide
- 1 : case normale grise
- 2 : case normale blanche (tourner gauche)
- 3 : case normale noire/grise (tourner droite)
*/

/* --- Formalisme messages --- ⚠
- { m: 'Touche moi et je disparais', l: 'l', t: 't', s: 500 }
- m: messages
- l: position left, 'l' pour aleatoire
- t: position top, 't' pour aleatoire
- s: temps avant prochain message (ms)
*/

// assets
// Fichier Manifest des images
var imagesManifest = [
	// Mur
	{id: 'wall1', src: './assets/img/wall1.png'},
	{id: 'wall2', src: './assets/img/wall2.png'},
	{id: 'wall3', src: './assets/img/wall3.png'},

	// Balises
	{id: 'begin1', src: './assets/img/begin1.png'},
	{id: 'begin2', src: './assets/img/begin2.png'},
	{id: 'end', src: './assets/img/end.png'},

	// Bots
	{id: 'bot1', src: './assets/img/dd_red.png'},
	{id: 'bot2', src: './assets/img/dd_blue.png'},
	{id: 'box', src: './assets/img/box.png'},
	{id: 'bot1box', src: './assets/img/dd_red_box.png'},
	{id: 'bot2box', src: './assets/img/dd_blue_box.png'},

	// Teleporteurs
	{id: 'TIn1', src: './assets/img/teleport_violet_in.png'},
	{id: 'TOut1', src: './assets/img/teleport_violet_out.png'},
	{id: 'TIn2', src: './assets/img/teleport_yellow_in.png'},
	{id: 'TOut2', src: './assets/img/teleport_yellow_out.png'},

	// Plaque Porte
	{id: 'plaque1On', src: './assets/img/pression_blue_on.png'},
	{id: 'plaque1Off', src: './assets/img/pression_blue_off.png'},
	{id: 'porte1Close', src: './assets/img/barrier_blue_off.png'},
	{id: 'porte1Open', src: './assets/img/barrier_blue_on_animated.png'},

	{id: 'plaque2On', src: './assets/img/pression_green_on.png'},
	{id: 'plaque2Off', src: './assets/img/pression_green_off.png'},
	{id: 'porte2Close', src: './assets/img/barrier_green_off.png'},
	{id: 'porte2Open', src: './assets/img/barrier_green_on_animated.png'},

	{id: 'plaque3On', src: './assets/img/pression_red_on.png'},
	{id: 'plaque3Off', src: './assets/img/pression_red_off.png'},
	{id: 'porte3Close', src: './assets/img/barrier_red_off.png'},
	{id: 'porte3Open', src: './assets/img/barrier_red_on_animated.png'},

	{id: 'plaque4On', src: './assets/img/pression_orange_on.png'},
	{id: 'plaque4Off', src: './assets/img/pression_orange_off.png'},
	{id: 'porte4Close', src: './assets/img/barrier_orange_off.png'},
	{id: 'porte4Open', src: './assets/img/barrier_orange_on_animated.png'},

	// Explosion
	{id: 'explosion', src: './assets/img/explosion.png'}
];

var soundsManifest = [
	{id: 'forward', src: './assets/sounds/forward.mp3'}, // Son avance
	{id: 'rotate', src: './assets/sounds/rotate.mp3'},   // Son pivote
	{
	  id: 'succes1',
	  src: './assets/sounds/triumphal_fanfare.mp3'
	}, // Son victoire
	{
	  id: 'succes2',
	  src: './assets/sounds/Jingle_victoire.mp3'
	},														  
	{id: 'explosion', src: './assets/sounds/explosion.mp3'}, // Son explosionD
	{id: 'Collabots', src: './assets/sounds/CollaBots.mp3'}  // Musique du jeu
];

// Variables globales
var nb_case_width  = 5;
var nb_case_height = 7;
var width, height;
var images, sounds;
//var nbLoadComplete	= 0;
//var nbLoadCompleteMax = 2;
var stage;
var boolCommandsOk;
var cvs1, cvs2, cvs1Back, cvs2Back;
var taillew, tailleh;
var menu_offset = 0;
var isPlaying = 0;      // 0 : jeu en pause (pas de render dans la boucle de jeu) ; 1 : jeu en cours 

// Config
var multiplicateurPolice = 0.1; // Taille de la police en fonction de la hauteur
var messageFinLevel		 = 'Félicitations !!!';
var messageUnPasFini	 = 'Un sur deux !';
var tempsEntreLevel		 = 3000; // (ms)
var tempsEntreUpdate	 = 1000; // (ms)
var messageErreurExec	= 'Finissez avant de lancer';
var messageNote			 = ['Tu peux mieux faire !', 'Presque !', 'Parfait !'];
var fps					 = 60;
var afficheMessages		 = false;
var cptRender			 = 0;
var vitesseBot			 = 600; // ms deplacement 1 case
var angleDandine		 = 30;
var vitesseRotationTele  = 2000; // ms pour un tour
var tempsDecalageBot	 = tempsEntreUpdate / 2;
// Gestion des sons
var activerMusique		 = true;
var activerBruitages	 = true;
var rotatecvs2			 = false;
// Audio 
var music                = null;


/*
Classe Robot gérant un bot,

@param position, position sur le fond
@param fond, lien vers le fond du robot
@param angle, angle du robot
*/
function Robot(position, fond, angle, cote)
{
	this.cote	 = cote;	 // Cvs 1 ou 2
	this.position = position; // Position robot
	this.toDo	 = [];		  // Action à faire
	this.sizeToDo = 0;		  // nombre action
	this.pointeur = 0;		  // Pointeur instruction courante
	this.fond	 = fond;	 // Fond damier du robot

	this.angle	 = {ca: angle, ta: angle, st: 0}; // Angle
	this.state	 = 0;			   // 0:arret, 1: mouvement, 2:pivote
	this.vector	= {x: 0, y: 0}; // Vecteur de deplacement
	this.animation = {
		cX: Math.floor(this.position[1] * taillew + taillew / 2),
		cY: Math.floor(this.position[0] * tailleh + tailleh / 2),
		tX: Math.floor(this.position[1] * taillew + taillew / 2),
		tY: Math.floor(this.position[0] * tailleh + tailleh / 2),
		time: Date.now(),
		angle: 0,
		sens: 1
	};					  // Position de base
	this.hasBox  = false; // si a une boite
	this.isAlive = true;  // Si en vie
}

/*
Remet à zero le robot, la position, les angles, ...
*/
Robot.prototype.reset = function(level) {
	var bot = this.cote - 1;
	this.position =
		new Array(levels[level].begin[bot][0], levels[level].begin[bot][1]);
	this.pointeur	 = 0;
	this.state		  = 0;
	this.vector.x	 = 0;
	this.vector.y	 = 0;
	this.animation.cX = this.animation.tX =
		Math.floor(this.position[1] * taillew + taillew / 2);
	this.animation.cY = this.animation.tY =
		Math.floor(this.position[0] * tailleh + tailleh / 2);

	this.angle.ca = levels[level].angle[bot];
	this.angle.ta = levels[level].angle[bot];
	this.angle.st = 0;
	this.hasBox   = false;
	this.isAlive  = true;
};

/*
Appelle la fonction correspondante selon l'algorithme
*/
Robot.prototype.update = function() {
	afficherPiles(this);
	this.isAlive = !this.isOnDoorClosed();
	if (this.pointeur < this.toDo.length && this.isAlive)
	{
		var action = this.toDo[this.pointeur];

		// Appelle la bonne fonction d'apres le tableau des instructions
		switch (action)
		{
		case 'avancer': this.forward(); break;
		case 'rotateRight': this.rotateR(); break;
		case 'rotateLeft': this.rotateL(); break;
		case 'pick': this.pick(); break;
		}
		this.pointeur += 1;
	}
};

/*
Fait 'avancer' le robot, appelle la bonne fonction
d'apres l'angle
*/
Robot.prototype.forward = function() {
	// Appelle la bonne fonction d'apres l'angle
	switch (Math.floor(this.angle.ca / 90))
	{
	case 0: this.up(); break;
	case 1: this.right(); break;
	case 2: this.down(); break;
	case 3: this.left(); break;
	}
};

/*
Deplace le robot d'une case vers le haut si possible
*/
Robot.prototype.up = function() {
	// Test
	// Trop haut
	if (this.position[0] <= 0)
		return;

	// Vide
	var newPos = [this.position[0] - 1, this.position[1]];
	if (this.fond[newPos[0]][newPos[1]] == 0)
		return;

	// Si va vers sur porte fermée
	this.isOnDoorClosed(newPos);

	// Okay
	this.position		= newPos;
	this.state			= 1;
	this.animation.tY   = this.animation.cY - tailleh;
	this.vector.x		= 0;
	this.vector.y		= -1;
	this.animation.time = Date.now();
	if (activerBruitages)
	{
		createjs.Sound.play('forward');
	}
};

/*
Deplace le robot d'une case vers la droite si possible
*/
Robot.prototype.right = function() {
	// Test
	// Trop droite
	if (this.position[1] == nb_case_width - 1)
		return;

	// Vide
	var newPos = [this.position[0], this.position[1] + 1];
	if (this.fond[newPos[0]][newPos[1]] == 0)
		return;

	// Si va vers sur porte fermée
	this.isOnDoorClosed(newPos);

	this.position		= newPos;
	this.state			= 1;
	this.animation.tX   = this.animation.cX + taillew;
	this.vector.x		= 1;
	this.vector.y		= 0;
	this.animation.time = Date.now();
	if (activerBruitages)
	{
		createjs.Sound.play('forward');
	}
};

/*
Deplace le robot d'une case vers le bas si possible
*/
Robot.prototype.down = function() {
	// Test
	// Trop bas
	if (this.position[0] == nb_case_height - 1)
		return;

	// Case vide
	var newPos = [this.position[0] + 1, this.position[1]];
	if (this.fond[newPos[0]][newPos[1]] == 0)
		return;

	// Si va vers sur porte fermée
	this.isOnDoorClosed(newPos);

	// Okay
	this.position		= newPos;
	this.state			= 1;
	this.animation.tY   = this.animation.cY + tailleh;
	this.vector.x		= 0;
	this.vector.y		= 1;
	this.animation.time = Date.now();
	if (activerBruitages)
	{
		createjs.Sound.play('forward');
	}
};

/*
Deplace le robot d'une case vers la gauche si possible
*/
Robot.prototype.left = function() {
	// Test
	// Trop gauche
	if (this.position[1] == 0)
		return;

	// Vide
	var newPos = [this.position[0], this.position[1] - 1];
	if (this.fond[newPos[0]][newPos[1]] == 0)
		return;

	// Si va vers sur porte fermée
	this.isOnDoorClosed(newPos);

	this.position		= newPos;
	this.state			= 1;
	this.animation.tX   = this.animation.tX - taillew;
	this.vector.x		= -1;
	this.vector.y		= 0;
	this.animation.time = Date.now();
	if (activerBruitages)
	{
		createjs.Sound.play('forward');
	}
};

/*
Pivote vers la gauche
*/
Robot.prototype.rotateL = function() {
	if (activerBruitages)
	{
		createjs.Sound.play('rotate');
	}
	this.state			= 2;
	this.angle.ta		= (this.angle.ta + 270) % 360;
	this.angle.st		= 'L';
	this.animation.time = Date.now();
};

/*
Pivote vers la droite
*/
Robot.prototype.rotateR = function() {
	if (activerBruitages)
	{
		createjs.Sound.play('rotate');
	}
	this.state			= 2;
	this.angle.ta		= (this.angle.ta + 90) % 360;
	this.angle.st		= 'R';
	this.animation.time = Date.now();
};

/*
Attrape la boite si le bot est sur une boite
et depose s'il en a une sur lui
*/
Robot.prototype.pick = function() {
	// Si le niveau contient des boites
	if (stage.boxes)
	{
		// Si a pas deja une boite
		if (!this.hasBox)
		{
			var box, count = 0, len = stage.boxes.length;
			// On teste pour toutes les boites
			for (count; count < len; count++)
			{
				box = stage.boxes[count];
				// Si bot sur une boite au sol
				if (box.cvs == this.cote && box.i == this.position[1] &&
					box.j == this.position[0] && box.droped == true)
				{
					// Boite dans le robot
					this.hasBox = box;
					// Boite plus au sol
					box.droped = false;
				}
			}
			}
		else // Si a deja une boite
		{
			var box, count = 0, len = stage.boxes.length;
			// On teste pour toutes les boites
			// si pas deja boite sur la case au sol
			for (count; count < len; count++)
			{
				box = stage.boxes[count];
				if (box.cvs == this.cote && box.i == this.position[1] &&
					box.j == this.position[0] && box.droped == true)
					return;
			}

			// Si libre
			this.hasBox.droped = true;			   // Boite au sol
			this.hasBox.i	  = this.position[1]; // Boite pos du bot
			this.hasBox.j	  = this.position[0];
			this.hasBox		   = false; // Bot plus de boite
		}
	}
};

/*
Affiche le robot selon les coordonnées et l'angle et gere l'animation
*/
Robot.prototype.render = function() {
	// Si bot arrivé position
	if (this.state > 0 && (Date.now() - this.animation.time) >= vitesseBot)
	{
		this.state			 = 0;
		this.vector.x		 = 0;
		this.vector.y		 = 0;
		this.animation.cX	= this.animation.tX;
		this.animation.cY	= this.animation.tY;
		this.animation.angle = 0;
		this.angle.ca		 = this.angle.ta;
		this.angle.st		 = 0;
		}

	// Si en mouvement modifie position
	if (this.state == 1)
	{
		var now	= Date.now();
		var deltaT = vitesseBot - (now - this.animation.time);
		var div;
		if (this.vector.x != 0)
		{
			div = taillew / vitesseBot;
			this.animation.cX =
				Math.floor(this.animation.tX - (this.vector.x * div * deltaT));
			}

		if (this.vector.y != 0)
		{
			div = tailleh / vitesseBot;
			this.animation.cY =
				Math.floor(this.animation.tY - (this.vector.y * div * deltaT));
			}

		var nbDandine = 2; // NE PAS CHANGER

		deltaT	= now - this.animation.time;
		div		  = angleDandine * 2 * nbDandine / vitesseBot;
		var total = deltaT * div;
		var util  = (total % (angleDandine * 2 * nbDandine));

		//[0, 20[
		if (util >= 0 && util <= angleDandine)
		{
			this.animation.angle = util;
			}
		// [20, 40[
		else if (util > angleDandine && util <= angleDandine * 2)
		{
			this.animation.angle = angleDandine - (util - angleDandine);
			}
		//[40, 60[
		else if (util > angleDandine * 2 && util <= angleDandine * 3)
		{
			this.animation.angle = (util - angleDandine * 2) * -1;
			}
		//[60, 80[
		else
		{
			this.animation.angle = ((angleDandine * 4) - util) * -1;
		}
		// console.log(this.animation.angle);
		// console.log('util' + util + ' div' + div + ' total' + total);
		// console.log(total);
		}

	// Si pivote modifie angle
	if (this.state == 2)
	{
		var deltaT = vitesseBot - (Date.now() - this.animation.time);
		var div	= 90 / vitesseBot;
		// console.log(deltaT);
		if (this.angle.st == 'R')
		{
			this.angle.ca = (this.angle.ta - (deltaT * div)) % 360;
			}
		if (this.angle.st == 'L')
		{
			this.angle.ca = (this.angle.ta + (deltaT * div)) % 360;
		}
		}

	if (this.isAlive)
	{
		// Image
		var bot2image;
		if (this.sizeToDo != 0 && this.toDo[this.pointeur] != null)
		{
			bot2image = this.chooseImageResult(this.toDo[this.pointeur]);
			}
		else
		{
			bot2image = (this.cote == '1') ? 'bot1' : 'bot2';
			if (this.hasBox != false)
				bot2image += 'box';
		}
		img = images.getResult(bot2image);
		}
	else
	{
		img = images.getResult('explosion');
		}

	// Affichage
	if (this.cote == 1)
	{
		cvs1.save();
		cvs1.translate(this.animation.cX, this.animation.cY);
		cvs1.rotate((this.angle.ca + this.animation.angle) * Math.PI / 180);
		cvs1.drawImage(
			img, 0, 0, img.width, img.height, -taillew / 2, -tailleh / 2,
			taillew, tailleh);
		cvs1.restore();
		}
	else
	{
		cvs2.save();
		cvs2.translate(this.animation.cX, this.animation.cY);
		cvs2.rotate((this.angle.ca + this.animation.angle) * Math.PI / 180);
		cvs2.drawImage(
			img, 0, 0, img.width, img.height, -taillew / 2, -tailleh / 2,
			taillew, tailleh);
		cvs2.restore();
	}
};

/*
Fonction qui choisi la bonne image d'apres l'action à executer
*/
Robot.prototype.chooseImageResult = function() {
	var toReturn = 'bot' + this.cote;
	if (this.hasBox != false)
		toReturn += 'box';

	return toReturn;
};

/*
Teste si va sur une porte ou si est sur une porte
@param newPos, optionnel nouvelle position
*/
Robot.prototype.isOnDoorClosed = function(newPos) {
	// Si on teste pas sur une nouvelle position
	// On teste sur la position actuelle
	if (newPos === undefined)
		newPos = this.position;

	if (levels[stage.level].duoPreDoor)
	{
		var img, duo, count = 0, len = stage.duoPreDoor.length;
		for (count; count < len; count++)
		{
			duo = stage.duoPreDoor[count];

			if (duo.dcvs == this.cote && newPos[0] == duo.dj &&
				newPos[1] == duo.di && duo.pOn == 0)
			{
                setTimeout(function() {
                    //stage.afficherMessage(messageExplosion);
				    this.isAlive = false;
				    if (activerBruitages) {
					   createjs.Sound.play('explosion');
					}
                }.bind(this), vitesseBot / 2);
				return true;
			}
		}
		}
	return false;
}

Robot.prototype.killRobot = function() {
    this.isAlive = false;
    
}



/*
Classe gérant les duo de plaque pression et portes
*/
function DuoPreDoor(pcvs, pi, pj, pOn, dcvs, di, dj, dAngle, id)
{
	this.pcvs   = pcvs;
	this.pi		= pi;
	this.pj		= pj;
	this.pOn	= pOn;
	this.dcvs   = dcvs;
	this.di		= di;
	this.dj		= dj;
	this.dAngle = dAngle;
	this.id		= id;
}

DuoPreDoor.prototype.pressionRender = function() {
	// Plaque
	// Choix img porte ouverte/fermer
	var nam = 'plaque' + this.id;
	if (this.pOn)
		img = images.getResult(nam + 'On');
	else
		img = images.getResult(nam + 'Off');

	// Affichage sur le bon canvas
	affichageRotate(img, 0, this.pcvs, this.pi, this.pj);
};

DuoPreDoor.prototype.porteRender = function() {
	// Porte
	// Choix img porte ouverte/fermer
	var nam = 'porte' + this.id;
	if (this.pOn)
	{
		img = images.getResult(nam + 'Close');
		affichageRotate(img, this.dAngle, this.dcvs, this.di, this.dj);
		}
	else
	{
		img = images.getResult(nam + 'Open');
		affichageRotateAnimated(img, this.dAngle, this.dcvs, this.di, this.dj, img.width / 5);
	}
};

DuoPreDoor.prototype.update = function(botG, botD, boxes) {
	// Si bot sur la plaque
	if (this.pcvs == 1)
	{
		this.pOn = (botG.position[1] == this.pi && botG.position[0] == this.pj);
		}
	else
	{
		this.pOn = (botD.position[1] == this.pi && botD.position[0] == this.pj);
		}

	// Si niveau contient boites
	if (boxes)
	{
		// Si box sur une plaque, on teste pour toutes
		// les plaques
		var box, count = 0, len = boxes.length;
		for (count; count < len; count++)
		{
			box = boxes[count];
			this.pOn |= (box.droped && this.pcvs == box.cvs && this.pi == box.i && this.pj == box.j)
		}
	}
};

DuoPreDoor.prototype.reset = function(l) {
	this.pOn = l.pOn;
};

/*
Classe representant un couple de teleporteur, entrée et sortie

*/
function Teleporter(inCvs, inI, inJ, angleIn, outCvs, outI, outJ, angleOut, id)
{
	this.inCvs	= inCvs;
	this.inI	  = inI;
	this.inJ	  = inJ;
	this.angleIn  = angleIn;
	this.outCvs   = outCvs;
	this.outI	 = outI;
	this.outJ	 = outJ;
	this.angleOut = angleOut;
	this.id		  = id;
	this.time	 = Date.now();
	this.active   = true;
}

Teleporter.prototype.render = function() {
	var now	= Date.now();
	var deltaT = now - this.time;

	if (deltaT >= vitesseRotationTele)
	{
		this.time	= now;
		this.angleIn = this.angleOut = 0;
		}

	// Fait tourner si actif
	if (this.active)
	{
		var div = - 360 / vitesseRotationTele;
		// Pivote selon deltat
		this.angleIn  = div * deltaT;
		this.angleOut = div * deltaT;
	}

	// Affichage le teleporteur IN
	img = 'TIn' + this.id;
	img = images.getResult(img);
	affichageRotate(img, this.angleIn, this.inCvs, this.inI, this.inJ);

	// Affichage le teleporteur IOUT
	img = 'TOut' + this.id;
	img = images.getResult(img);
	affichageRotate(img, this.angleOut, this.outCvs, this.outI, this.outJ);
};



/*
Affiche dans le bon canvas, l'image pivotéé ou non
*/
function affichageRotateTeleporteur(img, angle, cote, i, j)
{
    var cvs;
    switch (cote) {
        case 1: cvs = cvs1; break;
        case 2: cvs = cvs2; break;
        case 10: cvs = cvs1Back; angle = 0; break;
        case 20: cvs = cvs2Back; angle = 0; break;
    }

    var ratio = img.width / img.height;
    var angleR = angle * Math.PI / 180;
    
    if (angle == 0) {
        cvs.drawImage(img, 0, 0, img.width, img.height, i * taillew, j * tailleh, taillew, tailleh);
    }
    else {
        cvs.save();
		cvs.translate(i * taillew + taillew / 2, j * tailleh + tailleh / 2);
        cvs.rotate(angle * Math.PI / 180);
		cvs.drawImage(img, 0, 0, img.width, img.height, -taillew/2, -tailleh / 2, taillew, tailleh);
        cvs.restore();
    }
}


/*
Metà jour les teleporteurs et les boites i besoin
@param boxes, lien vers les boites du niveau
*/
Teleporter.prototype.update = function(boxes) {
	// Regarde toutes les boites
	if (this.active)
	{
		var box, count = 0, len = boxes.length;
		for (count; count < len; count++)
		{
			box = boxes[count];
			if (box.cvs == this.inCvs && box.droped == true && box.i == this.inI &&
				box.j == this.inJ) // Si boite sur entre tele
			{
				// Si la sortie est libre
				var libre = true;

				// Si boite sur la sortie
				var box2, count2 = 0, len2 = boxes.length;
				for (count2; count2 < len2 && libre; count2++)
				{
					box2 = boxes[count2];
					if (box2.cvs == this.outCvs && box2.droped == true && box2.i == this.outI &&
						box2.j == this.outJ) // Si boite sur sortie tele
						libre = false;
					}

				// Si libre on deplace la box
				if (libre)
				{
					box.cvs		= this.outCvs;
					box.i		= this.outI;
					box.j		= this.outJ;
					this.active = false;
				}
			}
		}
	}
};

Teleporter.prototype.reset = function(l) {
	this.active = true;
};


/*
Classe gerant les boites

*/

function Boxe(cvs, i, j, droped)
{
	this.cvs	= cvs;
	this.i		= i;
	this.j		= j;
	this.droped = droped;
}

Boxe.prototype.render = function() {
	// Affiche la boite si elle est au sol
	if (this.droped)
	{
		var img = images.getResult('box');
		affichageRotate(img, 0, this.cvs, this.i, this.j);
	}
};

Boxe.prototype.reset = function(l) {
	this.cvs	= l.cvs;
	this.i		= l.i;
	this.j		= l.j;
	this.droped = l.droped;
}


/*
Classe gérant les niveaux

@param level, le numéro du level
*/

function Stage(level)
{
	this.decal = 0;
	if (level >= levels.length)
	{
		console.log('!!! level ' + level + ' existe pas !!!');
        return;
	}
	this.level = level;
	var l	  = levels[level];

	// Copie de levels
	this.begin		   = JSON.parse(JSON.stringify(l.begin));
	this.end		   = JSON.parse(JSON.stringify(l.end));
	this.nbActionTotal = JSON.parse(JSON.stringify(l.nbActionTotal));
	this.messages	  = JSON.parse(JSON.stringify(l.messages));

	this.fondG = JSON.parse(JSON.stringify(l.gauche));
	this.fondD = JSON.parse(JSON.stringify(l.droite));

	this.botG = new Robot(
		new Array(this.begin[0][0], this.begin[0][1]), this.fondG, l.angle[0],
		1);
	this.botD = new Robot(
		new Array(this.begin[1][0], this.begin[1][1]), this.fondD, l.angle[1],
		2);
	this.time = Date.now();


	// Crée les elements de jeu, porte, boites, ...
	this.createElements(l);

	// Nettoie l'affichage
	this.cleanMessages();
	resetCommands();

	// Affiche premier message
	this.afficher(0);

	this.updateTeleporters(); // Met à jour les teleporteurs
	this.updateDuoPreDoor();  // Met à jours les plaques
	this.renderBackground();  // Fait un rendu du fond

    if (l.instructions) {
        // virage à droite
        document.getElementById("rotateRight1").style.visibility = (l.instructions.indexOf("rotateRight") >= 0) ? "visible" : "hidden";
        document.getElementById("rotateRight2").style.visibility = (l.instructions.indexOf("rotateRight") >= 0) ? "visible" : "hidden";
        // virage à gauche
        document.getElementById("rotateLeft1").style.visibility = (l.instructions.indexOf("rotateLeft") >= 0) ? "visible" : "hidden";
        document.getElementById("rotateLeft2").style.visibility = (l.instructions.indexOf("rotateLeft") >= 0) ? "visible" : "hidden";
        // attendre
        document.getElementById("rest1").style.visibility = (l.instructions.indexOf("rest") >= 0) ? "visible" : "hidden";
        document.getElementById("rest2").style.visibility = (l.instructions.indexOf("rest") >= 0) ? "visible" : "hidden";
        // ramasser
        document.getElementById("pick1").style.visibility = (l.instructions.indexOf("pick") >= 0) ? "visible" : "hidden";
        document.getElementById("pick2").style.visibility = (l.instructions.indexOf("pick") >= 0) ? "visible" : "hidden";
    }
    else {
        document.getElementById("rotateRight1").style.visibility = "visible";
        document.getElementById("rotateRight2").style.visibility = "visible";
        // virage à gauche
        document.getElementById("rotateLeft1").style.visibility = "visible";
        document.getElementById("rotateLeft2").style.visibility = "visible";
        // attendre
        document.getElementById("rest1").style.visibility = "visible";
        document.getElementById("rest2").style.visibility = "visible";
        // ramasser
        document.getElementById("pick1").style.visibility = "visible";
        document.getElementById("pick2").style.visibility = "visible";
    } 
        
	// menu_region = Math.floor(this.level / 5);
};

/*
Fait un unique rendu du background pour tout le niveau
*/
Stage.prototype.renderBackground = function() {
	// Efface tout
	cvs1Back.clearRect(0, 0, cvs1Back.width, cvs1Back.height);
	cvs2Back.clearRect(0, 0, cvs2Back.width, cvs2Back.height);

	// Affichage du fond -------------------
	for (var i = 0; i < nb_case_height; i++)
	{
		for (var j = 0; j < nb_case_width; j++)
		{
			if (this.fondG[i][j] != 0)
			{
				img = images.getResult('wall' + this.fondG[i][j]);
				affichageRotate(img, 0, 10, j, i);
				}

			if (this.fondD[i][j] != 0)
			{
				img = images.getResult('wall' + this.fondD[i][j]);
				affichageRotate(img, 0, 20, j, i);
			}
		}
	}

	// Affichage borne debut et fin
	// Debut
	img = images.getResult('begin1');
	affichageRotate(img, 0, 10, this.begin[0][1], this.begin[0][0]);
	img = images.getResult('begin2');
	affichageRotate(img, 0, 20, this.begin[1][1], this.begin[1][0]);

	// Fin
	img = images.getResult('end');
	affichageRotate(img, 0, 10, this.end[0][1], this.end[0][0]);
	affichageRotate(img, 0, 20, this.end[1][1], this.end[1][0]);
};

/*
Fonction qui vrifie si les deux robots sont sur la case de fin,
apres la fin de l'execution
*/
Stage.prototype.isFinish = function() {
	// Teste si robot sont sur leur position de fin
	if (this.botG.position[0] == this.end[0][0] &&
		this.botG.position[1] == this.end[0][1] &&
		this.botD.position[0] == this.end[1][0] &&
		this.botD.position[1] == this.end[1][1])
	{
		this.finish();
        // log
        if (xp) xp.logFinExecution("OK","OK");
		}
    else 
	// Si botG arrivé mais pas botD
	if (this.botG.position[0] == this.end[0][0] &&
		this.botG.position[1] == this.end[0][1] &&
		(this.botD.position[0] != this.end[1][0] ||
		 this.botD.position[1] != this.end[1][1]))
	{
		this.afficherMessage(messageUnPasFini);
        // log
        if (xp) xp.logFinExecution("OK","KO");
		}
    else 
	// Si botD arrivé mais pas botG
	if (this.botD.position[0] == this.end[1][0] &&
		this.botD.position[1] == this.end[1][1] &&
		(this.botG.position[0] != this.end[0][0] ||
		 this.botG.position[1] != this.end[0][1]))
	{
		this.afficherMessage(messageUnPasFini);
        // log
        if (xp) xp.logFinExecution("KO","OK");
	}
    else {
        // log
        if (xp) xp.logFinExecution("KO","KO");
        this.reset();
    }
    
	clearInstructions(); // Efface les instructions
    
    // masque la barre au dessus
    menu.showCurrentInstructions(false);
    
	// Autorise la modification des ordres
	boolCommandsOk = true;
};

/*
Fonction en cas de victoire, calcul la note du niveau et la note dans le
localstorage
*/
Stage.prototype.finish = function() {
	boolCommandsOk = false;
	var note =
		(this.botG.toDo.length + this.botD.toDo.length) / this.nbActionTotal;

	if (note > 0 && note <= 1) {
		note = 3;
    }
	else if (note > 1 && note <= 1.5) {
		note = 2;
    }
	else {
        note = 1
    }
        
	localStorage.setItem('note_level' + this.level, note);

	// Joue un son suivant la note
	if (note == 3 && activerBruitages)
		createjs.Sound.play('succes1');
	else
		createjs.Sound.play('succes2');

	this.afficherMessageFin(note);
};

/*
Fonction qui execute l'algoritme, reset
et appelle la fonction update qui se charge d'executer
*/
Stage.prototype.compute = function() {
	// Remet a zero position, angle, fond, boites, ...
	this.reset();
	// Ecrit dans le log les instructions
	this.log();
	// Et appelle la fonction apres temporisation
	window.setTimeout(function() { stage.update(); }, tempsEntreUpdate);
};

/*
Fonction qui reset, remet le fond de base,
l'angle et la position des robots à celle decrit dans le level
avec tous les objets
*/
Stage.prototype.reset = function() {
	var l = levels[this.level];
	// Fait des copies
	// Damier de fond
	this.fondG = JSON.parse(JSON.stringify(l.gauche));
	this.fondD = JSON.parse(JSON.stringify(l.droite));

	// Reset bots
	this.botG.reset(this.level);
	this.botD.reset(this.level);

	// Reset elements
	this.resetElements();
};

/*
Fonction qui execute l'algorithme en appellant les fonctions updates
des 2 bots, fait un rendu et se rappelle si besoin
*/
Stage.prototype.update = function() {
	// Interdit la modification des ordres
	boolCommandsOk = false;

	// Update les bots une fois sur 2
	if (this.decal == 0)
	{
		if (this.botG.isAlive)
			this.botG.update();
		}
	else
	{
		if (this.botD.isAlive)
			this.botD.update();
	}

	// Update les teleporteurs
	this.updateTeleporters();

	// Update les plaques et les barrieres
	this.updateDuoPreDoor();

	// Si apres avoir update bot G et D
	if (this.decal == 1)
	{
		// Procaine execution
		this.decal = (this.decal + 1) % 2;

		// Si il reste ordre et les deux bots vivants
		if ((this.botG.toDo.length > this.botG.pointeur ||
			 this.botD.toDo.length > this.botD.pointeur) &&
			this.botG.isAlive && this.botD.isAlive)
		{
			// On met a jour apres un délai
			window.setTimeout(function() { stage.update(); }, tempsEntreUpdate);
			}

		// Si les deux bots ont plus d'instruction ou que l'un des deux est mort
		if ((this.botG.toDo.length == this.botG.pointeur &&
			 this.botD.toDo.length == this.botD.pointeur) ||
			!this.botD.isAlive || !this.botG.isAlive)
		{
			// Test si fini apres temp
			window.setTimeout(function() {
				stage.isFinish();
			}, tempsEntreLevel / 2);
		}
		}
	else
	{
		// Appelle l'update pour mettre a jour le bot D
		this.decal = (this.decal + 1) % 2;
		window.setTimeout(function() { stage.update(); }, tempsDecalageBot);
	}
};

/*
Efface les canvas et réaffiche le fond, les bots, ...
Et appelles les fonction sd'affichage des bots
*/
Stage.prototype.render = function() {
	cptRender = (cptRender + 1) % 60;
	var img;
	// Calcule taille de chaque image

	// Clean les canvas
	cvs1.clearRect(0, 0, cvs1.width, cvs1.height);
	cvs2.clearRect(0, 0, cvs2.width, cvs2.height);

	// Teleporteurs
	this.teleporterRender();

	// Plaques pressions
	this.pressionRender();

	// Boites
	this.boxesRender();

	// Affichage des bots
	this.botG.render();
	this.botD.render();

	// Portes
	this.porteRender();
};


/*
Affiche les plaques et les portes
*/
Stage.prototype.teleporterRender = function() {
	if (this.teleporters)
	{
		var img, tele, count = 0, len = this.teleporters.length;
		for (count; count < len; count++)
		{
			this.teleporters[count].render();
		}
	}
};

/*
Crée les teleporteurs d'apres la description des niveaux

@param l, variable du level dans la config
*/
Stage.prototype.createElements = function(l) {
	var count, len;

	// Generation objet couple teleporteur
	if (l.teleporters)
	{
		len				 = l.teleporters.length;
		this.teleporters = new Array();
		for (count = 0; count < len; count++)
		{
			tele = l.teleporters[count];
			this.teleporters.push(new Teleporter(
				tele.inCvs, tele.inI, tele.inJ, tele.angleIn, tele.outCvs,
				tele.outI, tele.outJ, tele.angleOut, count + 1));
		}
		}

	// Generation objet des boites
	if (l.boxes)
	{
		len		   = l.boxes.length;
		this.boxes = new Array();
		for (count = 0; count < len; count++)
		{
			boxe = l.boxes[count];
			this.boxes.push(new Boxe(boxe.cvs, boxe.i, boxe.j, boxe.droped));
		}
		}

	// Generation objet duo pression door
	if (l.duoPreDoor)
	{
		len				= l.duoPreDoor.length;
		this.duoPreDoor = new Array();
		for (count = 0; count < len; count++)
		{
			duo = l.duoPreDoor[count];
			this.duoPreDoor.push(new DuoPreDoor(
				duo.pcvs, duo.pi, duo.pj, duo.pOn, duo.dcvs, duo.di, duo.dj,
				duo.dAngle, count + 1));
		}
	}
};

/*
Reset all elements
*/
Stage.prototype.resetElements = function() {
	var count, len, l = levels[this.level];

	// Generation objet couple teleporteur
	if (this.teleporters)
	{
		len = this.teleporters.length;
		for (count = 0; count < len; count++)
		{
			this.teleporters[count].reset(l);
		}
		}

	// Generation objet des boites
	if (this.boxes)
	{
		len = this.boxes.length;
		for (count = 0; count < len; count++)
		{
			this.boxes[count].reset(l.boxes[count]);
		}
		}

	// Generation objet duo pression door
	if (this.duoPreDoor)
	{
		len = this.duoPreDoor.length;
		for (count = 0; count < len; count++)
		{
            this.duoPreDoor[count].reset(l);
			//this.duoPreDoor[l.duoPreDoor[count]].reset(l);
		}
	}

};

/*
Met à jour les plaques de pression et les portes
*/
Stage.prototype.updateDuoPreDoor = function() {
	if (this.duoPreDoor)
	{
		// Parcourt tous les duo de plaque barriere
		var count = 0, len = this.duoPreDoor.length;
		for (count; count < len; count++)
		{
			if (this.boxes)
				(this.duoPreDoor[count])
					.update(this.botG, this.botD, this.boxes);
			else
				(this.duoPreDoor[count]).update(this.botG, this.botD);
		}
	}
};

/*
Affiche les plaques de pression
*/
Stage.prototype.pressionRender = function() {
	if (this.duoPreDoor)
	{
		var count = 0, len = this.duoPreDoor.length;
		for (count; count < len; count++)
		{
			this.duoPreDoor[count].pressionRender();
		}
	}
};

/*
Affiche les portes
*/
Stage.prototype.porteRender = function() {
	if (this.duoPreDoor)
	{
		var img, duo, count = 0, len = this.duoPreDoor.length;
		for (count; count < len; count++)
		{
			this.duoPreDoor[count].porteRender();
		}
	}
};


/*
Affiche les boites si sur le sol
*/
Stage.prototype.boxesRender = function() {
	// S'il y a des boites
	if (this.boxes)
	{
		var count = 0, len = this.boxes.length;
		// Parcourt les boites
		for (count; count < len; count++)
		{
			this.boxes[count].render();
		}
	}
};

/*
Met à jour les teleporteurs, transfere ou non les boites
*/
Stage.prototype.updateTeleporters = function() {
	if (this.teleporters && this.boxes)
	{
		// Regarde tous les teleporteurs
		var count = 0, len = this.teleporters.length;
		for (count; count < len; count++)
		{
			this.teleporters[count].update(this.boxes);
		}
	}
};

/*
Permet d'afficher un 'joli' message, le num_ieme effacable en cliquant
dessus
d'apres la description du level
@param num, numero du message a afficher
*/
Stage.prototype.afficher = function(num) {
	var l, t;
	// Si left pas definie = random
	var l = (this.messages[num].l == 'l') ?
				Math.floor(Math.random() * 70) + 10 + '%' :
				this.messages[num].l;

	// Si top pas defini = random
	var t = (this.messages[num].t == 't') ?
				Math.floor(Math.random() * 70) + 10 + '%' :
				this.messages[num].t;

    var mes = document.getElementById("message");
    
    mes.style.left = "50%";
    mes.style.top = "50%";   
    mes.innerHTML = this.messages[num].m;
    
	// Supprime le message et affiche le prochain s'il y a
	document.getElementById("bcMessages").onclick = function(e) {
		
        // Appelle fonction qui affiche prochain apres tempo
        if (num + 1 < levels[stage.level].messages.length) {
            //window.setTimeout(function() {            // closure retirée pour éviter les bugs inattendus d'affichage
                stage.afficher(num + 1);
            //}, levels[stage.level].messages[num].s);
        }
        else {
            document.getElementById("bcMessages").style.display = "none";
        }
	};
    
    document.getElementById("bcMessages").style.display = "block";
};

/*
Supprime tous les messages
*/
Stage.prototype.cleanMessages = function() {
	document.getElementById("bcMessages").style.display = "none";    
};

/*
Affiche un message position aleatoire effacable au clic
@param message, texte du message
*/
Stage.prototype.afficherMessage = function(message) {
	var l   = Math.floor(Math.random() * 60) + 10 + '%';
	var t   = Math.floor(Math.random() * 60) + 10 + '%';
	var mes = document.getElementById("message");
    mes.style.left = "50%";
	mes.style.top  = "50%";
    mes.innerHTML = message;
    
    document.getElementById("bcMessages").onclick = function(e) { this.style.display = "none"; stage.reset(); };
    
    document.getElementById("bcMessages").style.display = "block";
};

/*
Affiche le message de fin de niveau avec la note et sauvegarde le nombre d
etoile
@param note, note donnée pour la resolution du niveau
*/
Stage.prototype.afficherMessageFin = function(note) {
	// Création du message

	var mes = document.getElementById('message');
    var h = messageFinLevel 
        + "<p><img src='./assets/img/etoile" + (note < 1 ? "Off" : "") + ".png' class='etoile' style='width: 50px;'>"  
        + "<img src='./assets/img/etoile" + (note < 2 ? "Off" : "") + ".png' class='etoile' style='width: 50px;'>"  
        + "<img src='./assets/img/etoile" + (note < 3 ? "Off" : "") + ".png' class='etoile' style='width: 50px;'></p>"
        //+ "<p>" + messageNote[note - 1] + "</p>" 
        + "<p><img id='btnAgain' src='./assets/img/btnAgain.png' onclick='menu.loadLevel(" + this.level + ")'>";
    if (levels[this.level+1]) {
        h += "<img id='btnNext' src='./assets/img/btnNext.png' onclick='menu.loadLevel(" + (this.level+1) + ")'>";
    }
    h += "</p>";
    mes.innerHTML = h;
    document.getElementById("btnAgain").onclick = function(e) {
        e.stopImmediatePropagation();
        stage.reset();
        document.getElementById("bcMessages").style.display = "none";
    }.bind();
    document.getElementById("btnNext").onclick = function(e) {
        e.stopImmediatePropagation();
        menu.loadLevel(this.level+1);
    }.bind(this);
    document.getElementById("bcMessages").onclick = function(e) { };
    document.getElementById("bcMessages").style.display = "block";
    mes.style.left = "50%";
    mes.style.top = "50%";
};

/*
Ecrit dans le log les instructions saisies
*/
Stage.prototype.log = function() {
	/*
	var fileSystem = new ActiveXObject('Scripting.FileSystemObject');
	var monfichier = fileSystem.OpenTextFile('log.txt', 8, true);

	monfichier.WriteLine('Level ' + this.level + ':');
	monfichier.WriteLine(this.botG.toDo);
	monfichier.WriteLine(this.botD.toDo);
	*/
	var G = D = [];
	this.botD.toDo.forEach(function(e) {
		switch (e)
		{
		case 'avancer': D.push('↑'); break;
		case 'rotateRight': D.push('↷'); break;
		case 'rotateLeft': D.push('↶'); break;
		case 'pick': D.push('✋'); break;
		case 'rest': D.push('z'); break;
		}
	});
	this.botG.toDo.forEach(function(e) {
		switch (e)
		{
		case 'avancer': G.push('↑'); break;
		case 'rotateRight': G.push('↷'); break;
		case 'rotateLeft': G.push('↶'); break;
		case 'pick': G.push('✋'); break;
		case 'rest': G.push('z'); break;
		}
	});
	var str = 'Level ' + this.level + ':\nG:' + G + '\nD:' + D;
	// console.log(str);
};

if (! String.prototype.includes) {
    String.prototype.includes = function(s) {
        return this.indexOf(s) >= 0;
    }
}


/*
Fonction pour gerer les actions en fonction des touches voir readme
@param key, touche pressée
*/
function keyPush(key)
{
    return;
    
	// Changement de niveau
	var num = key.code.startsWith('Numpad') || key.code.startsWith("Digit");
	if (num)
	{
		num = parseInt(key.code.substr(-1));
		if (num != stage.level)
			stage = new Stage(num);
		}
	switch (key.key)
	{
	case 'a': stage = new Stage(10); break;
	case 'z': stage = new Stage(11); break;
	case 'e': stage = new Stage(12); break;
	case 'r': stage = new Stage(13); break;
	case 't': stage = new Stage(14); break;
	case 'y': stage = new Stage(15); break;
	case 'u': stage = new Stage(16); break;
	case 'i': stage = new Stage(17); break;
	case 'o': stage = new Stage(18); break;
	case 'p': stage = new Stage(19); break;
	case 'Escape':
		document.getElementById('menu_principal').style.display = 'none';
		break;
	}
}

/*
Quand on clqiue sur une instruction ajoute l'instruction au robot
et à la pile d'affichage
@param joueur, joueur concerné
@param balise, indique quelle image a été cliqué
*/
function changePictoToBeExec(joueur, balise)
{
    if (!boolCommandsOk) 
        return;
    
	// joueur pour savoir si gauche ou droite
	var img = new Image();
	var src = balise.src;

	// determiner l'action a effectuer
	if (joueur == 'gauche')
	{
		img.src			 = src;
		img.className	= balise.className;
		
        document.getElementById('j1exec').appendChild(img);

		// ajouter la commande au tableau toDo du robot A FAIRE EN FONCTION DE
		// LA CLASSE
		if (balise.className.includes('avancer'))
		{
			stage.botG.toDo[stage.botG.sizeToDo] = 'avancer';
			stage.botG.sizeToDo++;
			}
		else if (balise.className.includes('rotateRight'))
		{
			stage.botG.toDo[stage.botG.sizeToDo] = 'rotateRight';
			stage.botG.sizeToDo++;
			}
		else if (balise.className.includes('rotateLeft'))
		{
			stage.botG.toDo[stage.botG.sizeToDo] = 'rotateLeft';
			stage.botG.sizeToDo++;
			}
		else if (balise.className.includes('rest'))
		{
			stage.botG.toDo[stage.botG.sizeToDo] = 'rest';
			stage.botG.sizeToDo++;
			}
		else if (balise.className.includes('pick'))
		{
			stage.botG.toDo[stage.botG.sizeToDo] = 'pick';
			stage.botG.sizeToDo++;
		}
		}
	if (joueur == 'droite')
	{
		img.src			 = src;
		img.className	= balise.className;
		
        document.getElementById('j2exec').appendChild(img);

		// ajouter la commande au tableau toDo du robot A FAIRE EN FONCTION DE
		// LA CLASSE
		if (balise.className.includes('avancer'))
		{
			stage.botD.toDo[stage.botD.sizeToDo] = 'avancer';
			stage.botD.sizeToDo++;
			}
		else if (balise.className.includes('rotateRight'))
		{
			stage.botD.toDo[stage.botD.sizeToDo] = 'rotateRight';
			stage.botD.sizeToDo++;
			}
		else if (balise.className.includes('rotateLeft'))
		{
			stage.botD.toDo[stage.botD.sizeToDo] = 'rotateLeft';
			stage.botD.sizeToDo++;
			}
		else if (balise.className.includes('rest'))
		{
			stage.botD.toDo[stage.botD.sizeToDo] = 'rest';
			stage.botD.sizeToDo++;
			}
		else if (balise.className.includes('pick'))
		{
			stage.botD.toDo[stage.botD.sizeToDo] = 'pick';
			stage.botD.sizeToDo++;
		}
	}
	}

/*
Supprime la derniere instruction
@param joueur, indique le joueur gauche ou droite
*/
function cancelAction(joueur)
{
    if (!boolCommandsOk) 
        return;

	if (joueur == 'gauche')
	{
		var div   = document.getElementById('j1exec');
		var toRmI = div.children[div.children.length - 1];
		if (div.children.length != 0)
		{
			div.removeChild(toRmI);

			if (stage.botG.sizeToDo > 0)
			{
				stage.botG.toDo.splice(stage.botG.sizeToDo - 1);
				stage.botG.sizeToDo -= 1;
			}
		}
		}
	else
	{
		var div   = document.getElementById('j2exec');
		var toRmI = div.children[div.children.length - 1];
		if (div.children.length != 0)
		{
			div.removeChild(toRmI);

			if (stage.botD.sizeToDo > 0)
			{
				stage.botD.toDo.splice(stage.botD.sizeToDo - 1);
				stage.botD.sizeToDo -= 1;
			}
		}
	}
	}

/*
Supprime toutes les instructions
*/
function resetCommands()
{
	var div1 = document.getElementById('j1exec');
	var div2 = document.getElementById('j2exec');


	while (div1.hasChildNodes())
	{
		div1.removeChild(div1.lastChild);
		}


	while (div2.hasChildNodes())
	{
		div2.removeChild(div2.lastChild);
	}
	}

/*
Supprimer toutes les instructions pour un coté
@param div, div à Supprimer
*/
function resetSomeChildren(div)
{
	while (div.hasChildNodes())
	{
		div.removeChild(div.lastChild);
	}
	}

/*
Verifie si on peut lancer l'execution ou non
*/
function computeIfYouCan()
{
    if (!boolCommandsOk) {
        return ;
    }
        
    if (stage.botD.toDo.length == 0 && stage.botG.toDo.length == 0) {
        return;
    }
        
    isPlaying = true;
    
    // log
    if (xp) xp.logDebutExecution();
    
    // ferme les zones de saisie
	if (document.getElementById('j1commands').style.display == "block")
		menu.toggleCommands('j1commands');
	if (document.getElementById('j2commands').style.display == "block")
        menu.toggleCommands('j2commands');
    // montre le bandeau avec les instructions
    menu.showCurrentInstructions(true);
    // lance l'exécution
	stage.compute();
    // bloque l'edition des commandes
	boolCommandsOk = false;
}

/*
Supprime les instructions affiches a l'ecran
*/
function clearInstructions()
{
	var tmp1 = document.getElementById('instructionCourante1');
	var tmp2 = document.getElementById('instructionCourante2');
	resetSomeChildren(tmp1);
	resetSomeChildren(tmp2);
	}

/*
Affiche les instructions
@param bot, lien vers le robot
*/
function afficherPiles(bot)
{
	var pile1 = [];

	var tmp1 = document.getElementById('instructionCourante1');
	var tmp2 = document.getElementById('instructionCourante2');

	var borneSup = 0;
	if (bot.cote == 1)
	{
		resetSomeChildren(tmp1);
		}
	else
	{
		resetSomeChildren(tmp2);
		}

	if (bot.toDo.length - bot.pointeur <= 4)
	{
		borneSup = bot.toDo.length - bot.pointeur;
		}
	else
	{
		borneSup = 4;
		}

	for (var i = bot.pointeur; i < bot.pointeur + borneSup; i++)
	{
		var img2 = new Image();

		if (bot.cote == 1)
		{
			switch (bot.toDo[i])
			{
			case 'avancer': pile1[i] = './assets/img/forward_red.png'; break;
			case 'rotateLeft':
				pile1[i] = './assets/img/rotate_left_red.png';
				break;
			case 'rotateRight':
				pile1[i] = './assets/img/rotate_right_red.png';
				break;
			case 'rest': pile1[i] = './assets/img/rest_red.png'; break;
			case 'pick': pile1[i] = './assets/img/pickup_red.png'; break;
			}
			img2.src = pile1[i];

			tmp1.appendChild(img2);
			}
		else
		{
			switch (bot.toDo[i])
			{
			case 'avancer': pile1[i] = './assets/img/forward_blue.png'; break;
			case 'rotateLeft':
				pile1[i] = './assets/img/rotate_left_blue.png';
				break;
			case 'rotateRight':
				pile1[i] = './assets/img/rotate_right_blue.png';
				break;
			case 'rest': pile1[i] = './assets/img/rest_blue.png'; break;
			case 'pick': pile1[i] = './assets/img/pickup_blue.png'; break;
			}
			img2.src = pile1[i];

			tmp2.appendChild(img2);
		}
		}

}

/*
Affiche dans le bon canvas, l'image pivotée ou non
*/
function affichageRotate(img, angle, cote, i, j)
{
    var cvs;
    switch (cote) {
        case 1: cvs = cvs1; break;
        case 2: cvs = cvs2; break;
        case 10: cvs = cvs1Back; angle = 0; break;
        case 20: cvs = cvs2Back; angle = 0; break;
    }

    var ratio = img.width / img.height;
    var angleR = angle * Math.PI / 180;
    
    if (angle == 0) {
        cvs.drawImage(img, 0, 0, img.width, img.height, i * taillew, j * tailleh, taillew, tailleh);
    }
    else {
        cvs.save();
		cvs.translate(i * taillew + taillew / 2, j * tailleh + tailleh / 2);
        cvs.rotate(angle * Math.PI / 180);
		cvs.drawImage(img, 0, 0, img.width, img.height, -taillew/2, -tailleh / 2, taillew, tailleh);
        cvs.restore();
    }
}


/*
Affiche une image en tenant compte de l'animation
*/
function affichageRotateAnimated(img, angle, cote, i, j, tx) {
	var sx = Math.floor(cptRender / Math.floor(fps / 5)) * tx;

    var cvs;
    
    switch (cote) {
        case 1: cvs = cvs1; break;
        case 2: cvs = cvs2; break;
    }
    
    if (angle == 0) {
        cvs.drawImage(img, sx, 0, tx, img.height, i * taillew, j * tailleh, taillew, tailleh);
    }
	else {
        cvs.save();
        cvs.translate(i * taillew + taillew / 2, j * tailleh + tailleh / 2);
        cvs.rotate(angle * Math.PI / 180);
        cvs.drawImage(img, sx, 0, tx, img.height, -taillew / 2, -tailleh / 2, taillew, tailleh);
        cvs.restore();
    }
}


/*
Fonction generant le rendu
 http://creativejs.com/resources/requestanimationframe/
*/
function draw()
{
	window.requestAnimFrame(draw);
	if (isPlaying) {
        stage.render();
    }
}



var menu = (function() {


    /**
     *  [Private]
     *  Displays the page whose id is given in parameter. Hides all the others. 
     */
    var showPage = function(bcID) {
        // all front pages share the same class name ("bcPage")
        var bcPages = document.getElementsByClassName("bcPage");
        for (var i=0; i < bcPages.length; i++) {
            bcPages[i].style.display = (bcPages[i].id == bcID) ? "block" : "none";
        }
    }
     
    
    /** 
     *  Shows the title screen
     */ 
    this.showTitle = function() {
        isPlaying = false;
        showPage("bcTitle");
    }
        
    /** 
     *  Shows the option screen
     */
    this.showOptions = function() {
        isPlaying = false;
        showPage("bcOptions");
        this.updateChoixOptions();
    }

    /** 
     *  Shows the level selection screen
     */
    this.showLevels = function() {
        isPlaying = false;
        this.generateLevelMenu(levels[this.getCurrentLevel()].groupe);
        showPage("bcLevels");
    }
    
    /** 
     *  Shows the board screen
     */
    this.showBoard = function() {
        showPage("bcBoard");        
    }
    
    /** 
     *  Shows the credit screen
     */
    this.showCredits = function() {
        showPage("bcCredits");
    }
    

    /*************************************************************************
     *                          UTILITARY FUNCTIONS
     *************************************************************************/
    
    /** 
     *  Displays the title buttons
     */
    this.showTitleButtons = function() {
        document.getElementById("bcSubtitle").innerHTML = 
            "<span class='button' onclick='menu.showLevels(); if (xp) { xp.logDebutPartie(); }'>Jouer</span>" + 
            "<span class='button' onclick='menu.showOptions()'>Options</span>" + 
            "<span class='button' onclick='menu.showCredits()'>Crédits</span>";
    }

    /**
     *  Loads the current level
     */
    this.loadLevel = function(i) {
        // log
        if (xp) xp.logChoixNiveau(i+1);
        if (stage == null || stage.level != i) {
            stage = new Stage(i);
            document.getElementById("j1commands").style.display = "none";
            document.getElementById("j2commands").style.display = "none";
        }
        isPlaying = 1;
        this.showBoard();
    }
    
    /** 
     *  Computes the current level
     */
    this.getCurrentLevel = function() {
        return (stage == null) ? 1 : stage.level;
    }
    
    
    /** 
     *  Show/hide the commands
     */
    this.toggleCommands = function(id) {
        if (document.getElementById(id).style.display == "block") {
            document.getElementById(id).style.display = "none";
            // log
            if (xp) xp.logFermeturePanneau(id.charAt(1));
        }
        else {
            document.getElementById(id).style.display = "block";
            // log
            if (xp) xp.logOuverturePanneau(id.charAt(1));
        }
    }
    
    /** Show/hide the current instructions panel */
    this.showCurrentInstructions = function(b) {
        document.getElementById("instructionCourante1").style.display = (b) ? "block" : "none";
        document.getElementById("instructionCourante2").style.display = (b) ? "block" : "none";
    }
    
    
    /** Computes the level page content */
    this.generateLevelMenu = function(g) {

        var niveau = this.getCurrentLevel();
        var groupe = g;
        
        var subtitles = ["Déplacements", "Barrières", "Objets", "Téléporteurs"];
        var cesure = [3, 3, 3, 3];
        
        // titre
        var html = "<img src='./assets/img/choix.png' id='choix'>"; //Choix du niveau</h2>";
        
        //html += "<h3>" + subtitles[groupe-1] + "</h3>";
        
        // flèches gauches/droites + contenu
        if (groupe > 1) {
            html += '<img id="flechePrecedent" src="./assets/img/fleche_left.png" class="fleche" onclick="menu.generateLevelMenu(' + (groupe-1) + ')">';
        }
        if (groupe < subtitles.length) {
            html += '<img id="flecheSuivant" src="./assets/img/fleche_right.png" class="fleche" onclick="menu.generateLevelMenu(' + (groupe+1) + ')">';
        }
        
        html += '<div id="levelContent">';

        var nb = 0;
        // liste des niveaux
        for (var i=0; i < levels.length; i++) {
            if (levels[i].groupe == groupe) {
                nb++;
                var note = localStorage.getItem("note_level" + (i));
                html += "<div class='iconeNiveau";
                if (!note) {
                    html += " grise";
                    note = 0;
                }
                html += "' onclick='menu.loadLevel(" + (i) + ")'>" 
                    + "<img src='./assets/img/levels_icons/" + (i+1) + ".png' class='numero'><br>"
                    + "<img src='./assets/img/etoile" + (note < 1 ? "Off" : "") + ".png' class='etoile'>"  
                    + "<img src='./assets/img/etoile" + (note < 2 ? "Off" : "") + ".png' class='etoile'>"  
                    + "<img src='./assets/img/etoile" + (note < 3 ? "Off" : "") + ".png' class='etoile'>"  
                    + "</div>";
                if (nb == cesure[groupe-1]) {
                    html += "<br>";
                }
            }
        }
        
        html += '</div>' +
                '<div id="retour" onclick="menu.showTitle(); if (xp) { xp.logFinPartie(); }">Retour</div>';
        document.getElementById("bcLevels").innerHTML = html;
    }
    
    
    /**
     *  Called when an option is selected from the "Options" page
     */
    this.choixOption = function(elt) {
        switch (elt.id) {
            case "optMusiqueOui": 
                activerMusique = true;
                music.play();
                break;
            case "optMusiqueNon": 
                activerMusique = false;
                music.pause();
                break;
            case "optBruitagesOui": 
                activerBruitages = true;
                break;
            case "optBruitagesNon": 
                activerBruitages = false;
                break;
            case "optConfig1": 
                rotatecvs2 = false;
                document.getElementById("j2").style.transform = "none";
                document.getElementById("j2").style.WebKitTransform = "none";
                break;
            case "optConfig2": 
                rotatecvs2 = true;
                document.getElementById("j2").style.transform = "rotate(180deg)";
                document.getElementById("j2").style.WebKitTransform = "rotate(180deg)";
                break;
        }
        this.updateChoixOptions();
    }
    
    /** 
     *  Updates the display of the "Options" page to show the current status of the options
     */
    this.updateChoixOptions = function() {
        if (activerMusique) {
            document.getElementById("optMusiqueOui").style.fontSize = "150%";
            document.getElementById("optMusiqueNon").style.fontSize = "100%";
        }
        else {
            document.getElementById("optMusiqueOui").style.fontSize = "100%";
            document.getElementById("optMusiqueNon").style.fontSize = "150%";
        }
        if (activerBruitages) {
            document.getElementById("optBruitagesOui").style.fontSize = "150%";
            document.getElementById("optBruitagesNon").style.fontSize = "100%";
        }
        else {
            document.getElementById("optBruitagesOui").style.fontSize = "100%";
            document.getElementById("optBruitagesNon").style.fontSize = "150%";
        }
        if (rotatecvs2) {
            document.getElementById("optConfig1").style.fontSize = "100%";
            document.getElementById("optConfig2").style.fontSize = "150%";
        }
        else {
            document.getElementById("optConfig1").style.fontSize = "150%";
            document.getElementById("optConfig2").style.fontSize = "100%";
        }
    }
    
    
    // returns the current object
    return this;
            
})();




window.addEventListener('touchmove', function(e) { e.preventDefault(); });

window.addEventListener('resize', function(e) { initVar(); initCvs(); if (stage) { stage.renderBackground(); stage.render(); } })

// logs
var xp = null;

/*
Fonction s'executant en tout premier, servant d'initialisation,
de création des objets, de chargement des assets, de création des listener,
...
*/
function init() {
    console.log("init()");
	initVar();
	initCvs();
	initEvent();
	loadAssets();
}

/*
Fonction utilisant le preloader, chargant les assets imgs et sons
*/
function loadAssets()
{
    console.log("loading sounds...");
	// Chargement sound
	var sounds = new createjs.LoadQueue(false);
    createjs.Sound.registerPlugins([createjs.HTMLAudioPlugin]);
	sounds.installPlugin(createjs.Sound);
	createjs.Sound.alternateExtensions = ['mp3'];
    document.getElementById("bcSubtitle").innerHTML = "Chargement des sons...";
	sounds.addEventListener('complete', function() {
        // Chargement images
        console.log("loading graphics...");
	    images = new createjs.LoadQueue(false);
        document.getElementById("bcSubtitle").innerHTML = "Chargement des images...";
        images.addEventListener('complete', function() {
            menu.showTitleButtons();
            draw();
        });
        images.loadManifest(imagesManifest);        
        createjs.Sound.play("Collabots");
    });
	sounds.loadManifest(soundsManifest);
}

/*
initialise les variables globales
*/
function initVar()
{
	height = window.innerHeight;
	width  = window.innerWidth;    
    
	taillew = Math.floor(width / 2 / nb_case_width);
	tailleh = Math.floor(height / nb_case_height);

	// Taille police
	// document.body.style.fontSize = height * multiplicateurPolice + 'px';

	boolCommandsOk = true;
	}

/*
Initialise les canvas
*/
function initCvs()
{
	// Canvas
	cvs1	 = document.getElementById('cvs1').getContext('2d');
	cvs2	 = document.getElementById('cvs2').getContext('2d');
	cvs1Back = document.getElementById('cvs1_back').getContext('2d');
	cvs2Back = document.getElementById('cvs2_back').getContext('2d');

    var fullwidth = width/2;
    
    if (taillew > tailleh) 
        taillew = tailleh;
    else
        tailleh = taillew;
    
    height = tailleh * 7;
    width = taillew * 10;
    
    var left = fullwidth - width;
    
	// Set taille des canvas
	document.getElementById('cvs1_back').height = height;
	document.getElementById('cvs1_back').width  = (width / 2);
	cvs1Back.height								= height;
	cvs1Back.width								= width / 2;

	document.getElementById('cvs2_back').height = height;
	document.getElementById('cvs2_back').width  = (width / 2);
	cvs2Back.height								= height;
	cvs2Back.width								= width / 2;

	document.getElementById('cvs1').height = height;
	document.getElementById('cvs1').width  = (width / 2);
	cvs1.height							   = height;
	cvs1.width							   = width / 2;

	document.getElementById('cvs2').height = height;
	document.getElementById('cvs2').width  = (width / 2);
	cvs2.height							   = height;
	cvs2.width							   = width / 2;
}



/*
Fonction d'initialisation des evenements
*/
function initEvent()
{
	document.getElementById('boutonCommands1')
		.addEventListener('click', function(e) {
			menu.toggleCommands('j1commands');
			e.stopPropagation();
			e.preventDefault();
		});

	document.getElementById('boutonCommands2')
		.addEventListener('click', function(e) {
			menu.toggleCommands('j2commands');
			e.stopPropagation();
			e.preventDefault();
		});

	window.addEventListener('keyup', keyPush);

	// INIT DES EVENTS PICTO

	var pic1 = document.getElementsByClassName('pictoj1');
	for (var i = 0; i < pic1.length; ++i)
	{
		pic1[i].addEventListener('click', function(e) {
			changePictoToBeExec('gauche', this);
			e.stopPropagation();
			e.preventDefault();
		});
		}

	var pic2 = document.getElementsByClassName('pictoj2');

	for (var i = 0; i < pic2.length; ++i)
	{
		pic2[i].addEventListener('click', function(e) {
			changePictoToBeExec('droite', this);
			e.stopPropagation();
			e.preventDefault();
		});
	}

	// Bouton executer
	document.getElementById('boutonExec')
		.addEventListener('click', function(e) {
			computeIfYouCan();
			e.stopPropagation();
			e.preventDefault();
		});

	// Boutons annuler
	document.getElementById('cancel1').addEventListener('click', function(e) {
		cancelAction('gauche');
		e.stopPropagation();
		e.preventDefault();
	});
	document.getElementById('cancel2').addEventListener('click', function(e) {
		cancelAction('droite');
		e.stopPropagation();
		e.preventDefault();
	});

    // Bouton menu
	document.getElementById('menu_mobile')
		.addEventListener('click', function(e) {
			if (!boolCommandsOk) return;
            // log
            if (xp) xp.logRetourMenu();
            menu.showLevels();
			e.stopPropagation();
			e.preventDefault();
		});
    
    
    // messages 
    document.getElementById('bcMessages').onclick = function(e) {
       this.style.display = "none"; 
    };
    
}




/*
Fonction qui fait le rendu en fonction du navigateur, du systeme et des fps
*/
window.requestAnimFrame = (function() {     
    return window.requestAnimationFrame
 || window.webkitRequestAnimationFrame
 || window.mozRequestAnimationFrame
 || window.oRequestAnimationFrame
 || window.msRequestAnimationFrame
 || null; 
})(); 


init();

